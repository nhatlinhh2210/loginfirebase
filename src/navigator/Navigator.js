import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Login from '../screens/Login';
import Register from '../screens/Register';

const Stack = createStackNavigator();

function Navigator() {
  return (
    <Stack.Navigator>
      <Stack.Screen
       name="Đăng Nhập" 
       component={Login}
        options={
          {headerShown:false}} />
      <Stack.Screen
       name="Đăng Ký" 
       component={Register} />
    </Stack.Navigator>
  );
}
export default Navigator;
