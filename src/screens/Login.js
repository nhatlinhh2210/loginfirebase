import React from 'react';
import {     View, 
  Text, 
  TouchableOpacity, 
  TextInput,
  Platform,
  StyleSheet ,
  StatusBar,
  Alert, 
  Button} from 'react-native';
import * as Animatable from "react-native-animatable";
import LinearGradient from 'react-native-linear-gradient';
import Icon from "react-native-vector-icons/FontAwesome";
import Feather from "react-native-vector-icons/Feather";

// import { Container } from './styles';

function Login() {
  const [data,setData] = React.useState({
    email: '',
    password: '',
    check_textInputChange: false,
    secureTextEntry: true
  });
  const textInputChange = (val) =>{
    if (val.length !== 0){
      setData({
        ...data,
        email:val,
        check_textInputChange:true
      });
    }
    else {
      setData({
        ...data,
        email:val,
        check_textInputChange:false
      });
    }
  }
  const handlePasswordChange = (val) => {
    setData({
      ...data,
      password: val
    });
  }

  const updateSecureTextEntry = () => {
    setData({
      ...data,
      secureTextEntry: !data.secureTextEntry 
    });
  }

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.text_header}> Quản lý chung cư </Text>
      </View>     
      <View style={styles.footer}> 
        <Text style={styles.text_footer} > Đăng Nhập </Text>
        <Text style={styles.textTilte}> Email </Text>
        <View style={styles.action}>
          <Icon
            name="user-o"
            size={20}
            color="#000" 
            />
          <TextInput 
            placeholder="Tài khoản" 
            style={styles.textInput}
            placeholderTextColor="#666666"
            autoCapitalize="none"
            onChangeText={(val) => textInputChange(val)}
            />
          {data.check_textInputChange ? 
          <Animatable.View 
            animation="bounceIn"
          >
            <Feather 
              name="check-circle"
              color="green"
              size={20}
            />
          </Animatable.View>
          : null}
        </View>
        <Text style={styles.textTilte}> Mật Khẩu </Text>
        <View style={styles.action}>
          <Icon
            name="lock"
            size={20}
            color="#000" 
            />
          <TextInput 
            style={styles.textInput}
            placeholder="Mật khẩu" 
            autoCapitalize="none"
            secureTextEntry = {data.secureTextEntry ? true : false}
            
            onChangeText={(val) => handlePasswordChange(val)}
            />   
          <TouchableOpacity 
            onPress={updateSecureTextEntry}
          >

            {data.secureTextEntry ?
            <Feather
              name="eye-off"
              color="grey"
              size={20}
            />
          :
            <Feather
              name="eye"
              color="grey"
              size={20}
            />
            }
          </TouchableOpacity>

        </View>
        <View style={styles.button}>
          <LinearGradient 
            color={['#08d4c4', '#01ab9d']}
            style={styles.signIn}
          />
          <TouchableOpacity>
            <Text style={styles.textSign}>
                  Đăng nhập
            </Text>
          </TouchableOpacity>
            
        </View>
      </View>
    </View>

  );
}

export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    backgroundColor: '#009387'
  },
  header: {
      flex: 0,
      justifyContent: 'flex-end',
      paddingHorizontal: 20,
      paddingBottom: 30,
      paddingTop: 20,
      alignItems: "center",
  },
  footer: {
      flex: 3,
      backgroundColor: '#fff',
      borderTopLeftRadius: 30,
      borderTopRightRadius: 30,
      paddingHorizontal: 0,
      paddingVertical: 20,
  },
  text_header: {
      color: '#fff',
      fontWeight: 'bold',
      fontSize: 30
  },
  text_footer: {
      color: '#05375a',
      fontSize: 20,
      alignSelf:"center",
      
  },
  action: {
      flexDirection: 'row',
      marginTop: 10,
      borderBottomWidth: 1,
      borderBottomColor: '#f2f2f2',
      paddingBottom: 5,
      marginLeft: 20,
      marginRight: 20
  },
  actionError: {
      flexDirection: 'row',
      marginTop: 10,
      borderBottomWidth: 1,
      borderBottomColor: '#FF0000',
      paddingBottom: 5
  },
  textInput: {
      flex: 1,
      marginTop: Platform.OS === 'ios' ? 0 : -12,
      paddingLeft: 10,
      color: '#05375a',
  },
  errorMsg: {
      color: '#FF0000',
      fontSize: 14,
  },
  button: {
      alignItems: 'center',
      marginTop: 50,
      width: 200,
      height: 200
  },
  signIn: {
      width: '100%',
      height: 50,
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 10
  },
  textSign: {
      fontSize: 18,
      fontWeight: 'bold'
  },
  textTilte:{
    marginLeft: 10,
    color: '#05375a',
    fontSize: 15,
    marginTop: 20
  },
});